macro gaussian_blur_2d{

	// parse args
	args = parseArgs();

	// open the data
	open(args[0]);
	image = getTitle();

	sigma = args[1]
	scaled = args[2]

	scaledStr = "";
	if (scaled){
		scaledStr = "scaled"
	}

	// run
	run("Gaussian Blur...", "sigma=" + sigma + " " + scaledStr);

	// save result image
	saveAs("TIFF", args[3]);

}

function parseArgs(){
	argsStr = getArgument()
	argsStr = substring(argsStr, 1, lengthOf(argsStr)); // remove first char
	argsStr = substring(argsStr, 0, lengthOf(argsStr)-1); // remove last char
	print(argsStr);
	args = split(argsStr, ",");
	for (i=0 ; i < args.length ; i++){
		print(args[i]);
	}
	return args;
}

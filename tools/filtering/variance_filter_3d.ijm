macro variance_filter_3d{

	// parse args
	args = parseArgs();

	// open the data
	open(args[0]);
	image = getTitle();

	radiusX = args[1]
	radiusY = args[2]
	radiusZ = args[3]

	// run
	run("Variance 3D...", "x="+radiusX+" y="+radiusY+" z="+radiusZ);

	// save result image
	saveAs("TIFF", args[4]);

}

function parseArgs(){
	argsStr = getArgument()
	argsStr = substring(argsStr, 1, lengthOf(argsStr)); // remove first char
	argsStr = substring(argsStr, 0, lengthOf(argsStr)-1); // remove last char
	print(argsStr);
	args = split(argsStr, ",");
	for (i=0 ; i < args.length ; i++){
		print(args[i]);
	}
	return args;
}

FROM ubuntu:20.04

WORKDIR /app

COPY . /app
COPY ./tools /app

RUN apt-get update  && \
    apt-get install -y unzip && \
    apt-get install -y wget && \
    wget https://downloads.imagej.net/fiji/latest/fiji-linux64.zip && \
    unzip fiji-linux64.zip && \
    rm fiji-linux64.zip
    
ENV PATH="/app/Fiji.app:$PATH" 

CMD ["bash"]
